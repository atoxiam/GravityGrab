# Gravity Grab
Gravity Grab is a  game made for a 2 week production project on a team with other programmers for an AIE physics assessment
The game is a physics sandbox where you can use a jet pack, grappling hook, gravity gun, and gravity star to play in two different maps
One map is a physics item and explosives room where you can throw objects and explosives around with the gravity gun, and use the gravity star
The other map is a platformer map where you can utilize the jet pack and grappling hook to make it to the top

[our release is available on itch.io here](https://atoxiam.itch.io/gravity-grab)

# Controls
* WASD for movement
* Shift for jet pack
* E for grappling hook
* Q for gravity star
* Left Mouse click to hold or drop an item
* Right mouse click to throw an item your holding

# Team Members:
* Matthew Dowling
* Xavier Melton
* Martin Balch